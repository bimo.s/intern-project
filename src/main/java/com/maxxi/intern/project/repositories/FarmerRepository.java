package com.maxxi.intern.project.repositories;

import com.maxxi.intern.project.repositories.entities.Farmer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FarmerRepository extends JpaRepository<Farmer, String> {
}