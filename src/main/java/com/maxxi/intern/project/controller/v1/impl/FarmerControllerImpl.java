package com.maxxi.intern.project.controller.v1.impl;

import com.maxxi.intern.project.controller.v1.FarmerController;
import com.maxxi.intern.project.responses.BaseResponse;
import com.maxxi.intern.project.services.v1.FarmerService;
import com.maxxi.intern.project.utils.ResponseHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequiredArgsConstructor
public class FarmerControllerImpl implements FarmerController {

    private final FarmerService farmerService;

    @Override
    public ResponseEntity<BaseResponse> getListFarmer() {
        return ResponseHelper.buildOkResponse(farmerService.getFarmerList());
    }
}
