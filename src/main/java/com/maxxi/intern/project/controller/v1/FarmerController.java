package com.maxxi.intern.project.controller.v1;

import com.maxxi.intern.project.responses.BaseResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/farmer")
public interface FarmerController {

    @GetMapping
    ResponseEntity<BaseResponse> getListFarmer();
}
