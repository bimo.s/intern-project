package com.maxxi.intern.project.controller.advices;

import com.maxxi.intern.project.responses.BaseResponse;
import com.maxxi.intern.project.utils.ResponseHelper;
import com.maxxi.intern.project.utils.exceptions.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    protected ResponseEntity<BaseResponse> handleBadRequest(BadRequestException ex) {
        ex.printStackTrace();
        log.warn("handleBadRequest: {}", ex.getMessage());
        return ResponseHelper.buildBadRequestResponse(ex.getMessage());
    }

}
