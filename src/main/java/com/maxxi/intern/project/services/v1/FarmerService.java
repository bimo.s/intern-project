package com.maxxi.intern.project.services.v1;

import com.maxxi.intern.project.responses.FarmerResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FarmerService {

    List<FarmerResponse> getFarmerList();
}
