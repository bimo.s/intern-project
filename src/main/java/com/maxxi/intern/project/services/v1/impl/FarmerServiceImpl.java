package com.maxxi.intern.project.services.v1.impl;

import com.maxxi.intern.project.repositories.FarmerRepository;
import com.maxxi.intern.project.repositories.entities.Farmer;
import com.maxxi.intern.project.responses.FarmerResponse;
import com.maxxi.intern.project.services.v1.FarmerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FarmerServiceImpl implements FarmerService {

    private final FarmerRepository farmerRepository;

    @Override
    public List<FarmerResponse> getFarmerList() {
        List<Farmer> farmerList = farmerRepository.findAll();

        return farmerList.stream()
                .map(farmer -> FarmerResponse.builder()
                        .name(farmer.getName())
                        .id(farmer.getId())
                        .build())
                .collect(Collectors.toList());
    }
}
