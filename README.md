# Intern Project
<!--- These are examples. See https://shields.io for others or to customize this set of shields. You might want to include dependencies, project status and licence info here --->
![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
![IntelliJ IDEA](https://img.shields.io/badge/IntelliJIDEA-000000.svg?style=for-the-badge&logo=intellij-idea&logoColor=white)

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the Java Version 11
* You have a `<Windows/Linux/Mac>` machine.
* You have read [Jira Work](https://maxxitani.atlassian.net/l/cp/wE2Eqs0w).

## Installing Bitzer Dependencies

```
mvn clean install
```

Add run commands and examples you think users will find useful.
